from model import *

class Controller:

	def __init__(self, model, view):
		self.model = model
		self.view = view

	def handleCommand(self):
		self.view.render(True)
		commands = self._getCommands()
		keywords = commands + [ name for name, depth in self.model.getItemNamesList(self.view.displayedYear) ] + Model.MONTH_NAMES
		it = iter(self.view.getCommand(keywords).split(' '))
		command = it.next().strip()
		args = list(it)
		if not command in ['quit', 'exit']:
			if command:
				if command == '?':
					self.view.infoList = commands
					self.view.showInfo("Press any key to continue")
					self.view.infoList = []
				else:
					try:
						getattr(self, command)(*args)
					except Exception as e:
						self.view.showInfo("Error: %s. Press any key to continue" % str(e))
						del self.view.commandHistory[0]
			self.model.save()
			self.handleCommand()

	def add(self, *words):
		it = iter(words)
		type = it.next()
		args = list(it)
		if type == 'year':
			self._addYear(args)
		elif type == 'category':
			self._addCategory(args)
		elif type == 'row':
			self._addRow(args)
		else:
			self.view.showInfo("No such type: %s. Press any key to continue" % type)

	def _addYear(self, args):
		yearNumber = args[0]
		if yearNumber in self.model.years:
			self.view.showInfo("Year %s already exists. Type 'show %s' to switch to %s. Press any key to continue" % (yearNumber, yearNumber, yearNumber))
		else:
			currentYear = self.model.currentYear
			self.model.years[yearNumber] = Category(self.model.years[currentYear]) if currentYear else Category()

	def _addRow(self, words):
		if not self._verifyConflictingKeywords(words):
			return
		parentCategory, name = self._parseCategory(' '.join(words))
		if not self._verifyAmbigousNames(name):
			return
		displayedYear = self.model.years[self.view.displayedYear]
		category = displayedYear.getCategory(parentCategory)
		if category:
			category.addRow(name)
		else:
			displayedYear.addRow(name)

	def _addCategory(self, words):
		if not self._verifyConflictingKeywords(words):
			return
		parentCategory, name = self._parseCategory(' '.join(words))
		if not self._verifyAmbigousNames(name):
			return
		displayedYear = self.model.years[self.view.displayedYear]
		category = displayedYear.getCategory(parentCategory) if parentCategory else displayedYear
		category.addCategory(name)

	def _verifyConflictingKeywords(self, words):
		keywords = Model.MONTH_NAMES + ['all']
		conflictingKeywords = [ word for word in words if word in keywords ]
		if conflictingKeywords:
			return False
		return True

	def _verifyAmbigousNames(self, name):
		displayedYear = self.model.years[self.view.displayedYear]
		conflictingNames = self._getConflictingNames(name)
		if conflictingNames:
			self.view.showInfo("Ambigous name %s conflicts with %s. Press any key to continue" % (name, str(conflictingNames)))
			return False
		return True

	def _parseCategory(self, string):
		year = self.model.years[self.view.displayedYear]
		return self._parse(string, year.categoryNamesList)

	def _parseItem(self, string):
		return self._parse(string, [ name for (name, depth) in self.model.getItemNamesList(self.view.displayedYear) ])

	def _parse(self, string, sourceList):
		item = None
		for name in sorted(sourceList, key=len, reverse=True):
			try:
				name = name.encode('utf-8')
			except UnicodeDecodeError, UnicodeEncodeError:
				pass
			if string.startswith(name):
				item = name
				string = string.replace(name, '', 1).strip()
				break
		return item, string

	def inc(self, *words):
		name, value = self._parseItem(' '.join(words))
		self.model.addValue(name, float(value), self.view.displayedYear)

	def delete(self, *words):
		trailing = ' '.join(words)
		while trailing:
			name, trailing = self._parseItem(trailing)
			self.model.delete(name, self.view.displayedYear)

	def rename(self, *words):
		name, newName = self._parseItem(' '.join(words))
		self.model.rename(name, newName, self.view.displayedYear)

	def move(self, *words):
		name, newCategory = self._parseItem(' '.join(words))
		self.model.move(name, newCategory, self.view.displayedYear)

	def sheets(self):
		self.view.showInfoList(self.model.years.keys())

	def setmonth(self, *words):
		month = words[0].lower()
		try:
			self.model.currentMonth = [ m.lower() for m in Model.MONTH_NAMES ].index(month)
		except:
			pass

	def show(self, yearNumber):
		if yearNumber in self.model.years:
			self.model.currentYear = yearNumber
		else:
			self.view.showInfo("No such year: %s. Press any key to continue" % yearNumber)

	def put(self, *words):
		it = iter(words)
		itemName = it.next()
		displayedYear = self.model.years[self.view.displayedYear]
		try:
			while True:
				if itemName in [ name.encode('utf-8') for name in displayedYear.itemNames ]:
					break
				else:
					itemName += ' ' + it.next()
		except StopIteration:
			self.view.showInfo("Could not find row from input: %s. Press any key to continue" % itemName)
			return

		try:
			value = it.next()
			floatValue = float(value)
		except ValueError:
			self.view.showInfo("Given value %s could not be parsed to float. Press any key to continue" % value)
			return
		try:
			month = it.next()
		except StopIteration:
			month = None
		displayedYear = self.model.years[self.view.displayedYear]
		if displayedYear:
			month = displayedYear.currentMonth if not month else month
			if not month:
				self.view.showInfo("This year is fully marked. Please specify month to edit value. Press any key to continue")
			else:
				if month in displayedYear.months:
					self.model.edit(itemName, floatValue, self.view.displayedYear, month)
				else:
					self.view.showInfo("Invalid month: %s. Press any key to continue" % month)
		else:
			self.view.showInfo("No year to edit. Type 'add year <year>' to add a year. Press any key to continue")

	def _parseWords(self, words):
		displayedYear = self.model.years[self.view.displayedYear]
		months = [ word for word in words if word in Model.MONTH_NAMES ]
		if not months and 'all' in words:
			months = Model.MONTH_NAMES
			words.remove('all')

		words = [ word for word in words if word not in Model.MONTH_NAMES ]
		if words == ['all']:
			itemNames = displayedYear.itemNames
			return (months, itemNames, None)

		itemNames = []
		it = iter(words)
		itemName = it.next()
		try:
			while True:
				if itemName in [ name.encode('utf-8') for name in displayedYear.itemNames ]:
					itemNames.append(itemName)
					try:
						itemName = it.next()
					except StopIteration:
						itemName = None
						break
				else:
					itemName += ' ' + it.next()
		except StopIteration:
			trailingName = itemName

		return (months, itemNames, trailingName)

	def _parseFirstWord(self, words):
		displayedYear = self.model.years[self.view.displayedYear]
		months = [ word for word in words if word in Model.MONTH_NAMES ]
		if not months and 'all' in words:
			months = Model.MONTH_NAMES
			words.remove('all')

		words = [ word for word in words if word not in Model.MONTH_NAMES ]

		it = iter(words)
		itemName = it.next()
		try:
			while True:
				if itemName in [ name.encode('utf-8') for name in displayedYear.itemNames ]:
					trailingName = ' '.join(list(it))
					break
				else:
					itemName += ' ' + it.next()
		except StopIteration:
			trailingName = itemName
			itemName = ''

		return (months, itemName, trailingName)

	def _getConflictingNames(self, name):
		itemNamesList = self.model.getItemNamesList(self.view.displayedYear)
		conflictingNames = []
		for (itemName, depth) in itemNamesList:
			try:
				itemName = itemName.encode(self.view.code)
			except UnicodeDecodeError:
				pass
			if name.startswith(itemName + " ") or itemName.startswith(name + " "):
				conflictingNames.append(itemName)
		return conflictingNames

	def _getCommands(self):
		return [ method for method in dir(self) if callable(getattr(self, method)) and not method.startswith('_') and method != 'handleCommand' ]
