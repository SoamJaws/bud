import json
import os.path
import codecs
from shutil import copyfile
import sys
reload(sys)  
sys.setdefaultencoding('utf8')

class Model:

	MONTH_NAMES = [ 'Januari'
                  , 'Februari'
                  , 'Mars'
                  , 'April'
                  , 'Maj'
                  , 'Juni'
                  , 'Juli'
                  , 'Augusti'
                  , 'September'
                  , 'Oktober'
                  , 'November'
                  , 'December'
                  ]

	@staticmethod
	def emptyValues():
		return [0.0] * len(Model.MONTH_NAMES)

	def __init__(self, path):
		self.path = path
		self.years = dict()
		self.currentYear = None
		self.currentMonth = None

	def load(self):
		if not os.path.isfile(self.path):
			self.save()
		with open(self.path) as data_file:
			data = json.load(data_file)
		for yearNumber, yearData in data['years'].iteritems():
			year = Category()
			year.load(yearData)
			self.years[yearNumber] = year
		if 'currentYear' in data:
			self.currentYear = data['currentYear']
		if 'currentMonth' in data:
			self.currentMonth = data['currentMonth']

	def save(self):
		try:
			with open(self.path, 'w') as outfile:
				data = json.dump(self.serialize(), outfile, indent=4, ensure_ascii=False)
		except UnicodeEncodeError, UnicodeDecodeError:
			with codecs.open(self.path, 'w', encoding="utf-8") as outfile:
				data = json.dump(self.serialize(), outfile, indent=4, ensure_ascii=False)

	def serialize(self):
		return { 'years'        : { yearNumber : year.serialize() for yearNumber, year in self.years.iteritems() }
               , 'currentYear'  : self.currentYear
               , 'currentMonth' : self.currentMonth
               }

	def getItemNamesTree(self, yearName):
		year = self.years[yearName]
		itemNamesTree = []
		categories = [year]
		while categories:
			itemNames = []
			subcategories = []
			for category in categories:
				for cName, c in category.categories.iteritems():
					itemNames.append(cName)
					subcategories.append(c)
				for rName in category.rows:
					itemNames.append(rName)
			itemNamesTree.append(itemNames)
			categories = subcategories
		return itemNamesTree

	def getItemNamesDict(self, yearName):
		year = self.years[yearName]
		return year.getItemNamesDict(0)

	def getItemNamesList(self, yearName):
		if yearName:
			year = self.years[yearName]
			return year.getItemNamesList(0)
		else:
			return []

	def getRowNamesList(self, yearName):
		if yearName:
			year = self.years[yearName]
			return year.rowNamesList
		else:
			return []

	def getValues(self, name, yearName):
		year = self.years[yearName]
		item = year.getRow(name)
		if not item:
			item = year.getCategory(name)
		values = item.values
		return values + [sum(values)]

	def delete(self, name, yearName):
		year = self.years[yearName]
		parent = year.getParent(name)
		if name in parent.categories:
			del parent.categories[name]
		elif name in parent.rows:
			del parent.rows[name]

	def addValue(self, name, value, yearName):
		year = self.years[yearName]
		row = year.getRow(name)
		if row:
			row.values[self.currentMonth] += value

	def rename(self, name, newName, yearName):
		year = self.years[yearName]
		try:
			name = name.decode('utf-8')
		except UnicodeEncodeError:
			pass
		parent = year.getParent(name)
		try:
			newName = newName.decode('utf-8')
		except UnicodeDecodeError, UnicodeEncodeError:
			pass
		if name in parent.categories:
			parent.categories[newName] = parent.categories.pop(name)
		elif name in parent.rows:
			parent.rows[newName] = parent.rows.pop(name)

	def move(self, name, newCategory, yearName):
		year = self.years[yearName]
		try:
			name = name.decode('utf-8')
		except UnicodeEncodeError:
			pass
		parent = year.getParent(name)
		try:
			newCategory = newCategory.decode('utf-8')
		except UnicodeDecodeError, UnicodeEncodeError:
			pass
		category = year.getCategory(newCategory)
		if name in parent.categories:
			category.categories[name] = parent.categories.pop(name)
		elif name in parent.rows:
			category.rows[name] = parent.rows.pop(name)


class Category:

	def __init__(self, orig=None):
		self.categories = dict()
		self.rows = dict()
		if orig:
			for categoryName, category in orig.categories.iteritems():
				self.categories[categoryName] = Category(category)
			for rowName, row in orig.rows.iteritems():
				self.rows[rowName] = Row(row)

	def load(self, categoryData):
		for name, data in categoryData['categories'].iteritems():
			category = Category()
			category.load(data)
			self.categories[name] = category
		for name, data in categoryData['rows'].iteritems():
			row = Row()
			row.load(data)
			self.rows[name] = row

	def addCategory(self, name):
		try:
			name = name.decode('utf-8')
		except UnicodeDecodeError, UnicodeEncodeError:
			pass
		self.categories[name] = Category()

	def addRow(self, name):
		try:
			name = name.decode('utf-8')
		except UnicodeDecodeError, UnicodeEncodeError:
			pass
		self.rows[name] = Row()

	def getCategory(self, categoryName):
		try:
			categoryName = categoryName.decode('utf-8')
		except UnicodeEncodeError:
			pass
		if self.categories:
			if categoryName in self.categories:
				return self.categories[categoryName]
			else:
				for cName, c in self.categories.iteritems():
					category = c.getCategory(categoryName)
					if category:
						return category
		return None

	def getParent(self, name):
		if self.categories:
			if name in self.categories:
				return self
			else:
				for cName, c in self.categories.iteritems():
					category = c.getParent(name)
					if category:
						return category
		if name in self.rows:
			return self
		return None

	def getRow(self, rowName):
		try:
			rowName = rowName.decode('utf-8')
		except UnicodeEncodeError:
			pass
		if rowName in self.rows:
			return self.rows[rowName]
		if self.categories:
			for categoryName, category in self.categories.iteritems():
				row = category.getRow(rowName)
				if row:
					return row
		return None

	def serialize(self):
		return { 'categories' : { categoryName : category.serialize() for categoryName, category in self.categories.iteritems() },
	   			 'rows' : { rowName : row.serialize() for rowName, row in self.rows.iteritems() }
			   }

	def getItemNamesDict(self, depth):
		itemNames = {}
		for rowName, row in self.rows.iteritems():
			itemNames[(rowName, depth)] = None
		for categoryName, category in self.categories.iteritems():
			itemNames[(categoryName, depth)] = category.getItemNamesDict(depth+1)
		return itemNames

	def getItemNamesList(self, depth):
		itemNames = []
		for rowName, row in self.rows.iteritems():
			itemNames.append((rowName, depth))
		for categoryName, category in self.categories.iteritems():
			itemNames.append((categoryName, depth))
			itemNames += category.getItemNamesList(depth+1)
		return itemNames

	@property
	def categoryNamesList(self):
		categoryNames = []
		for categoryName, category in self.categories.iteritems():
			categoryNames.append(categoryName)
			categoryNames += category.categoryNamesList
		return categoryNames

	@property
	def rowNamesList(self):
		rowNames = []
		for rowName, row in self.rows.iteritems():
			rowNames.append(rowName)
		for categoryName, category in self.categories.iteritems():
			rowNames += category.rowNamesList
		return rowNames

	@property
	def values(self):
		categoryValues = Model.emptyValues()
		rowValues = Model.emptyValues()
		for categoryName, category in self.categories.iteritems():
			if not categoryValues:
				categoryValues = [0] * len(category.values)
			categoryValues = [ x + y for x, y in zip(categoryValues, category.values) ]
		for rowName, row in self.rows.iteritems():
			if not rowValues:
				rowValues = [0] * len(row.values)
			rowValues = [ x + y for x, y in zip(rowValues, row.values) ]
		return [ categoryValue + rowValue for categoryValue, rowValue in zip(categoryValues, rowValues) ]
class Row:

	def __init__(self, orig=None):
		if not orig:
			self.values = Model.emptyValues()
		else:
			self.values = orig.values

	def load(self, rowData):
		self.values = rowData

	def serialize(self):
		return self.values


class ModelError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

class YearNotDefinedError(ModelError):
	pass
