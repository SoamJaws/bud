import curses
import locale
from model import Model, Category
import sys

class View:

	def __init__(self, model):
		self.model = model
		self.screen = curses.initscr()
		self.height, self.width = self.screen.getmaxyx()
		self.topH = 4

		curses.start_color()
		curses.use_default_colors()
		self.colors = [ curses.color_pair(i) for i in range(61, 230, 4) ]
			
		for i in range(0, curses.COLORS):
			curses.init_pair(i + 1, i, -1)

		curses.noecho()
		self.screen.immedok(True)
		self.screen.keypad(1)

		self.displayedYear = model.currentYear
		self.infoList = []
		self.commandHistory = []
		self.scrollY = 0
		self.code = locale.getpreferredencoding()

	def getCommand(self, keywords):
		self.screen.move(self.height-2, 1 + len(self.prompt))
		command = ''
		xOffset = 0
		commandHistoryP = -1
		while(True):
			self._showPrompt(command)
			y, x = self.screen.getyx()
			self.screen.move(y, x-xOffset)
			event = self.screen.getch()
			refreshValues = False
			try:
				char = chr(event)
			except:
				char = ''

			# Enter
			if event in [curses.KEY_ENTER, 10]:
				if command.strip():
					self.commandHistory.insert(0, command)
				if len(self.commandHistory) > 100:
					del self.commandHistory[-1]
				commandHistoryP = -1
				self.infoList = []
				return command

			# Command navigation
			elif event in [curses.KEY_UP, 259]:
				if commandHistoryP < 99 and commandHistoryP < len(self.commandHistory) - 1:
					commandHistoryP += 1
					command = self.commandHistory[commandHistoryP]
					xOffset = 0
			elif event in [curses.KEY_DOWN, 258]:
				if commandHistoryP > -1:
					commandHistoryP -= 1
					command = self.commandHistory[commandHistoryP] if commandHistoryP > -1 else ''
					xOffset = 0
			elif event in [9]:
				words = command.split(' ')
				if words[-1] == '': # Command empty or ending with space, suggest next word
					self.showInfoList(keywords)
				else:
					suggestions = [ word for word in keywords if word.startswith(words[-1]) ]
					if len(suggestions) == 1:
						try:
							words[-1] = suggestions[0].encode(self.code)
						except UnicodeDecodeError:
							words[-1] = suggestions[0]
						command = ' '.join(words) + ' '
						self.showInfoList([])
					else:
						self.showInfoList(suggestions)

			# Sheet navigation
			elif event in [curses.KEY_PPAGE, 339]:
				if self.scrollY > 0:
					self.scrollY -= self.midH / 2
					refreshValues = True
				if self.scrollY < 0:
					self.scrollY = 0
			elif event in [curses.KEY_NPAGE, 338]:
				if self.displayedYear:
					itemNamesList = self.model.getItemNamesList(self.displayedYear)
					if len(itemNamesList) - self.scrollY > self.midH:
						self.scrollY += self.midH / 2
						refreshValues = True

			# Command edit
			elif event in [curses.KEY_LEFT, 260]:
				if x - xOffset > len(self.prompt) + 1:
					xOffset += 1
			elif event in [curses.KEY_RIGHT, 261]:
				if x - xOffset < len(self.prompt) + 1 + len(command):
					xOffset -= 1
			elif event in [curses.KEY_HOME, 262]:
				xOffset = len(command)
			elif event in [curses.KEY_END, 360]:
				xOffset = 0
			elif event in [curses.KEY_BACKSPACE, 127]:
				if xOffset < len(command):
					i = len(command)-xOffset
					command = command[:i-1] + command[i:]
			elif event in [23]: # Ctrl+w
				words = command.split(' ')
				if words[-1] == '':
					del words[-1]
				del words[-1]
				command = ' '.join(words)
				if len(command) > 0:
					command += ' '
			elif event in [curses.KEY_DC, 330]:
				if xOffset > 0:
					i = len(command)-xOffset
					command = command[:i] + command[i+1:]
					xOffset -= 1
			elif event in [27]:
				return ''
			else:
				i = len(command) - xOffset
				command = command[:i] + char + command[i:]
				self.screen.move(y, x+1)
			self.render(refreshValues)

	def showInfoList(self, infoList):
		self.infoList = infoList
		self.render(True)

	def showInfo(self, text):
		self.render(False)
		self._showPrompt(text)
		self.screen.getch()

	def render(self, refreshValues):
		if not self.displayedYear:
			self.displayedYear = self.model.currentYear

		if refreshValues:
			self.screen.clear()
		self.screen.border(0)

		self._renderTop()
		self._renderBot()
		self._renderColumns()
		if refreshValues:
			self._renderRowTitles()
			self._renderValues()
	
		self.screen.refresh()

	def _renderTop(self):
		# Sheet title
		year = str(self.displayedYear)
		self.screen.addstr(1, self.width/2-len(year)/2, year)

		# Column titles
		if self.displayedYear:
			for x in range(0, len(self.columnTitles)):
				if x == self.model.currentMonth:
					self.screen.addstr(self.topH-2, self.rowTitlesWidth+x*self.columnTitleWidth, self.columnTitles[x], self.colors[7])
				else:
					self.screen.addstr(self.topH-2, self.rowTitlesWidth+x*self.columnTitleWidth, self.columnTitles[x])

		# Top divider
		self.screen.hline(3, 1, curses.ACS_HLINE, self.width-2)
		self.screen.addch(3, 0, curses.ACS_LTEE)
		self.screen.addch(3, self.width-1, curses.ACS_RTEE)

	def _renderBot(self):
		botY = self.height-self.botH
		for y in range(0, len(self.infoList)):
			self.screen.addstr(botY+1+y, 1, ' ' * (self.width-2))
			info = self.infoList[y]
			self.screen.addstr(botY+1+y, 1, info)
		self.screen.hline(botY, 1, curses.ACS_HLINE, self.width-2)
		self.screen.addch(botY, 0, curses.ACS_LTEE)
		self.screen.addch(botY, self.width-1, curses.ACS_RTEE)

	def _renderColumns(self):
		if self.displayedYear:
			itemNamesList = self.model.getItemNamesList(self.displayedYear)
			start = self.scrollY
			stop = len(itemNamesList) if len(itemNamesList) - self.scrollY <= self.midH else self.scrollY + self.midH
			for x in range(0, len(self.columnTitles)):
				self.screen.vline(self.topH, self.rowTitlesWidth-1+self.columnTitleWidth*x, curses.ACS_VLINE, self.midH)
				self.screen.addch(self.topH-1, self.rowTitlesWidth-1+self.columnTitleWidth*x, curses.ACS_TTEE)
				self.screen.addch(self.topH+self.midH, self.rowTitlesWidth-1+self.columnTitleWidth*x, curses.ACS_BTEE)

	def _renderRowTitles(self):
		if self.displayedYear:
			itemNamesList = self.model.getItemNamesList(self.displayedYear)
			rowTitleXs = self.rowTitleXs
			start = self.scrollY
			stop = len(itemNamesList) if len(itemNamesList) - self.scrollY <= self.midH else self.scrollY + self.midH
			previousDepth = 0
			color = 0
			for y in range(start, stop):
				(rowTitle, depth) = itemNamesList[y]
				try:
					rowTitle = rowTitle.encode(self.code)
				except UnicodeDecodeError:
					pass
				if previousDepth != depth:
					previousDepth = depth
					color += 1
				x = 0 if not rowTitleXs else rowTitleXs[depth]
				self.screen.addstr(self.topH-self.scrollY+y, 1+x, rowTitle, self.colors[color%len(self.colors)])

	def _renderValues(self):
		if self.displayedYear:
			itemNamesList = self.model.getItemNamesList(self.displayedYear)
			start = self.scrollY
			stop = len(itemNamesList) if len(itemNamesList) - self.scrollY <= self.midH else self.scrollY + self.midH
			for x in range(0, len(self.columnTitles)):
				previousDepth = 0
				color = 0
				for y in range(start, stop):
					(rowTitle, depth) = itemNamesList[y]
					if previousDepth != depth:
						previousDepth = depth
						color += 1
					values = self.model.getValues(rowTitle, self.displayedYear)
					value = "%.2f" % values[x]
					self.screen.addstr(self.topH-self.scrollY+y, self.rowTitlesWidth-1+self.columnTitleWidth*x+(self.columnTitleWidth-len(value)), value, self.colors[color%len(self.colors)])

	def _showPrompt(self, text):
		self.screen.addstr(self.height-2, len(self.prompt)+1, ' ' * (self.width-(2+len(self.prompt+text))))
		self.screen.refresh()
		self.screen.addstr(self.height-2, 1, self.prompt + text)

	@property
	def prompt(self):
		prompt = str(self.displayedYear)
		if self.model.currentMonth != None:
			prompt += " - %s" % Model.MONTH_NAMES[self.model.currentMonth]
		prompt += ": "
		return prompt

	@property
	def botH(self):
		return 3 + len(self.infoList)

	@property
	def midH(self):
		return self.height - (self.topH + self.botH)

	@property
	def rowTitleXs(self):
		rowTitleXs = self.rowTitlesWidths
		rowTitleXs.insert(0, 0)
		del rowTitleXs[-1]
		for i in range(1, len(rowTitleXs)):
			rowTitleXs[i] += rowTitleXs[i-1]
		return rowTitleXs

	@property
	def rowTitlesWidth(self):
		return sum(self.rowTitlesWidths) + 3

	@property
	def rowTitlesWidths(self):
		itemNamesTree = self.model.getItemNamesTree(self.displayedYear)
		itemNamesTree = [ itemNames for itemNames in itemNamesTree if itemNames ]
		return [ len(max(itemNames, key=len)) + 1 for itemNames in itemNamesTree ]

	@property
	def columnTitlesWidth(self):
		return self.width - self.rowTitlesWidth + 2

	@property
	def columnTitleWidth(self):
		return self.columnTitlesWidth / len(self.columnTitles)

	@property
	def columnTitles(self):
		return Model.MONTH_NAMES + ['Totalt']
